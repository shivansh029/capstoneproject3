import ProtocolBuffer.BuildingOuterClass;
import ProtocolBuffer.EmployeeOuterClass;

import java.io.BufferedReader;
import java.io.FileReader;

public class CSVToProto {

    public void readCSVData(String filePath, String fileType){
        try{

            FileReader fr = new FileReader(filePath);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            boolean header = true;

            while((line = br.readLine()) != null){
                if(header){
                    header = false;
                    continue;
                }

                String[] data = line.split(",");

                if(fileType.equals("Employee")){
                    printEmployeeData(data);
                }
                else{
                    printBuildingData(data);
                }
            }

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private void printBuildingData(String[] buildingData) {

        if(buildingData.length == 0){
            return;
        }

        BuildingOuterClass.Building.Builder building = BuildingOuterClass.Building.newBuilder();
        building.setBuildingCode(buildingData[0])
                .setTotalFloors(Integer.parseInt(buildingData[1]))
                .setTotalCompanies(Integer.parseInt(buildingData[2]))
                .setCafeteriaCode(buildingData[3]);

        System.out.println(building.toString());
    }

    private void printEmployeeData(String[] employeeData) {

        if(employeeData.length == 0){
            return;
        }

        EmployeeOuterClass.Employee.Builder employee = EmployeeOuterClass.Employee.newBuilder();
        employee.setEmployeeID(Integer.parseInt(employeeData[0]))
                .setName(employeeData[1])
                .setBuildingCode(employeeData[2])
                .setFloorNumberValue(Integer.parseInt(employeeData[3]))
                .setSalary(Integer.parseInt(employeeData[4]))
                .setDepartment(employeeData[5]);

        System.out.println(employee.toString());
    }


}
