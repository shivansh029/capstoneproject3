public class Main {
    public static void main(String[] args) {

        String employeeCSVFilePath = "/Users/shivansh/Capstone Projects/capstoneproject3/src/main/resources/EmployeeData.csv";
        String buildingCSVFilePath = "/Users/shivansh/Capstone Projects/capstoneproject3/src/main/resources/BuildingData.csv";

        CSVToProto csvToProtoUtil = new CSVToProto();

        System.out.println("************************************************************************************");
        System.out.println("Employee Data:\n");
        csvToProtoUtil.readCSVData(employeeCSVFilePath, "Employee");
        System.out.println("************************************************************************************");
        System.out.println("\nBuilding Data:\n");
        csvToProtoUtil.readCSVData(buildingCSVFilePath,"Building");
    }
}
